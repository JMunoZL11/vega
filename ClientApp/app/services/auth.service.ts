import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelper } from "angular2-jwt";
import 'rxjs/add/operator/filter';
import * as auth0 from 'auth0-js';

@Injectable()
export class AuthService {

  profile: any;
  private roles: string[] = [];

  auth0 = new auth0.WebAuth({
    clientID: '3Nl0NvCvYiOT8jEBxQlqACtCi1e3JG8w',
    domain: 'johannproject.auth0.com',
    responseType: 'token id_token',
    audience: 'https://johannproject.auth0.com/userinfo',
    redirectUri: 'http://localhost:5000',      
    scope: 'openid'
  });

  constructor(public router: Router) {}

  public isInRole(roleName) {
    return this.roles.indexOf(roleName) > -1;
  }

  public login(): void {
    this.auth0.authorize();
  }

  public handleAuthentication(): void {
    this.readUserFromLocalStorage();

    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken) {
        window.location.hash = '';
        this.setSession(authResult);
        this.router.navigate(['/']);
      } else if (err) {
        this.router.navigate(['/']);
        console.log(err);
      }
    });
  }

  private setSession(authResult): void {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    // localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('token', authResult.accessToken);
    localStorage.setItem('expires_at', expiresAt);

    this.auth0.client.userInfo(authResult.accessToken, (error, profile) => {
      if (error)
        throw error;
      
      localStorage.setItem('profile', JSON.stringify(profile));
      this.readUserFromLocalStorage();
    });
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    // localStorage.removeItem('access_token');
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('profile');

    this.profile = null;
    this.roles = [];
    // Go back to the home route
    this.router.navigate(['/']);
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  private readUserFromLocalStorage() {
    this.profile = JSON.parse(localStorage.getItem('profile'));

    var token = localStorage.getItem('token');
    if (token) {
      var jwtHelper = new JwtHelper();
      var decodedTocken = jwtHelper.decodeToken(token);
      this.roles = decodedTocken['https://vega.com/roles'] || [];
    }
  }

}