import { AdminAuthGuardService } from './services/admin-auth-guard.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { BrowserXhrWithProgress, ProgressService } from './services/progress.service';
import { PhotoService } from './services/photo.service';
import * as Raven from "raven-js";

import { AppErrorHandler } from './app.error-handler';
import { ErrorHandler } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, BrowserXhr } from '@angular/http';
import { sharedConfig } from './app.module.shared';
import { VehicleService } from "./services/vehicle.service";
import { AUTH_PROVIDERS } from 'angular2-jwt';

Raven.config('https://811741172e9d4eb989f5df00a7f60421@sentry.io/216178').install();

@NgModule({
    bootstrap: sharedConfig.bootstrap,
    declarations: sharedConfig.declarations,
    imports: [
        BrowserModule,
        HttpModule,
        ...sharedConfig.imports
    ],
    providers: [
        { provide: 'ORIGIN_URL', useValue: location.origin },
        { provide: ErrorHandler, useClass: AppErrorHandler},
        VehicleService,
        PhotoService,
        AuthService,
        AuthGuardService,
        AdminAuthGuardService,
        AUTH_PROVIDERS
    ]
})
export class AppModule {
}
