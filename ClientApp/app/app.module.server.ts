import { AUTH_PROVIDERS } from 'angular2-jwt';
import { AdminAuthGuardService } from './services/admin-auth-guard.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { ProgressService } from './services/progress.service';
import { PhotoService } from './services/photo.service';
import * as Raven from "raven-js";

import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { sharedConfig } from './app.module.shared';
import { VehicleService } from "./services/vehicle.service";

Raven.config('https://811741172e9d4eb989f5df00a7f60421@sentry.io/216178').install();

@NgModule({
    bootstrap: sharedConfig.bootstrap,
    declarations: sharedConfig.declarations,
    imports: [
        ServerModule,
        ...sharedConfig.imports
    ],
    providers: [
        VehicleService,
        PhotoService,
        AuthService,
        AuthGuardService,
        AdminAuthGuardService,
        AUTH_PROVIDERS
    ]
})
export class AppModule {
}
